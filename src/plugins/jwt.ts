import { FastifyRequest, FastifyReply } from "fastify"
import fp from "fastify-plugin"

module.exports = fp(async (fastify: any, opts: any) => {
  fastify.register(require("@fastify/jwt"), {
    secret: opts.secret,
    sign: {
      expiresIn: "1 days",
      aud: opts.aud,
      iss: opts.iss
    },
  })

  fastify.decorate("authenticate", async function (request: FastifyRequest, reply: FastifyReply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })
})