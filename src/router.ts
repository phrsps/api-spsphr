import { FastifyInstance } from 'fastify';

export default async (fastify: FastifyInstance) => {
  fastify.register(require('./controllers/index-controllers'), { prefix: '/', logger: true });

  //Moph IC
  fastify.register(require('./controllers/mophic/get_token'), { prefix: '/moph', logger: true });

  // Route Export PHR Data
  fastify.register(require('./controllers/export-phr-controllers'), { prefix: '/phr', logger: true });
  // fastify.register(require('./controllers/export-phrtest-controllers'), { prefix: '/phrtest', logger: true });

  // Route Export Visit Data
  fastify.register(require('./controllers/export-visit-controllers'), { prefix: '/visit', logger: true });

  ///test
  // fastify.register(require('./controllers/phr-controllers'), { prefix: '/test', logger: true });


}
