import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import * as HttpStatus from 'http-status-codes';
import { ExportVisitModel } from '../models/export-visit-models'
import { ExportPhrModel } from '../models/export-phr-models'
import { GetTokenModel } from '../models/mophic/get_token'
import { GetPhrModel } from '../models/mophic/get_phr'

const fromImportModel = new ExportVisitModel();
const exportPhrModel = new ExportPhrModel();
const getTokenModel = new GetTokenModel();
const getPhrModel = new GetPhrModel();

export default async (fastify: FastifyInstance) => {
  const db = fastify.db;
  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(HttpStatus.StatusCodes.OK).send({ message: 'ExportVisitModel  -->Fastify, RESTful API services! HI720220220' });
  });

  fastify.post('/info_visit', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const hn = req.body.hn
    const dateStart = req.body.dateStart
    const dateEnd = req.body.dateEnd

    try {
      const rs: any = await fromImportModel.viewVisit(db, dateStart, dateEnd, hn);
      reply.status(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, results: rs[0] });
    } catch (error) {
      fastify.log.error(error);
      reply.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).send({ statusCode: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR, message: HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR) })
    }

  });

}