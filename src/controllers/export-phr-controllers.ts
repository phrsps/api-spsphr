import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import * as HttpStatus from 'http-status-codes';

import { ExportPhrModel } from '../models/export-phr-models'
import { GetTokenModel } from '../models/mophic/get_token'
import { GetPhrModel } from '../models/mophic/get_phr'

const fromImportModel = new ExportPhrModel();
const getTokenModel = new GetTokenModel();
const getPhrModel = new GetPhrModel();

export default async (fastify: FastifyInstance) => {
    const db = fastify.db;
    fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
        reply.code(HttpStatus.StatusCodes.OK).send({ message: 'ExportAipnModel  -->Fastify, RESTful API services! HI720220220' });
    });

    fastify.post('/test', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        try {
            var rs = await fromImportModel.getManagingOrganization(db);
            // console.log(rs);
            reply.code(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, results: rs });
        } catch (error: any) {
            console.log(HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR));
            reply.code(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).send({
                statusCode: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
                results: {
                    ok: false,
                    text: "การอ่านข้อมูลเกิดความผิดพลาด",
                    error: HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
                }
            });
        }
    });


    fastify.post('/export', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;

        const token: any = req.body.token;
        let rs: any = {};
        // let req_test ={
        //     hn:'1111',
        //     vn:'2222'
        //     token:'xxx'
        // }

        try {
            let managingOrganization: any = await fromImportModel.getManagingOrganization(db);
            // console.log(managingOrganization);
            let _managingOrganization_: any = managingOrganization.rows[0];

            let _managingOrganization: any = {
                "type": "Organization",
                "identifier": {
                    "use": "official",
                    "system": "https://bps.moph.go.th/hcode/5",
                    "value": _managingOrganization_.hospcode
                },
                "display": "โรงพยาบาล" + _managingOrganization_.hospname
            }

            let patient: any = await fromImportModel.getPatient(db, req);
            // console.log(patient);
            let _patient_: any = patient.rows[0];
            let _patient: any = {
                "identifier": [
                    {
                        "use": "official",
                        "system": "https://www.dopa.go.th",
                        "type": "CID",
                        "value": _patient_.cid,
                        "period": {
                            "start": _patient_.cid_start.toString()
                        }
                    },
                    {
                        "use": "official",
                        "system": "https://sil-th.org/hn",
                        "assigner": {
                            "use": "official",
                            "system": "https://bps.moph.go.th/hcode/5",
                            "value": _managingOrganization_.hospcode,
                            "display": "โรงพยาบาล" + _managingOrganization_.hospname
                        },
                        "type": "HN",
                        "value": _patient_.hn,
                        "period": {
                            "start": _patient_.visit_start.toString()
                        }
                    }
                ],
                "active": true,
                "name": [
                    {
                        "use": "official",
                        "text": _patient_.prename + _patient_.fname + " " + _patient_.lname,
                        "languageCode": "TH",
                        "family": _patient_.fname + " " + _patient_.lname,
                        "given": [
                            _patient_.fname
                        ],
                        "prefix": [
                            _patient_.prename
                        ],
                        "suffix": [
                            _patient_.lname
                        ],
                        "period": {
                            "start": _patient_.period_start
                        }
                    },
                    {
                        "use": "official",
                        "text": _patient_.engpname + "." + _patient_.engfname + " " + _patient_.englname,
                        "languageCode": "EN",
                        "family": _patient_.engfname + " " + _patient_.englname || '-',
                        "given": [
                            _patient_.engfname || "-"
                        ],
                        "prefix": [
                            _patient_.engpname || "-"
                        ],
                        "suffix": [
                            _patient_.englname || '-'
                        ],
                        "period": {
                            "start": _patient_.period_start
                        }
                    }
                ],
                "telecom": [
                    {
                        "system": "phone",
                        "value": _patient_.hometel,
                        "use": _patient_.phone_type,
                        "rank": "1",
                        "period": {
                            "start": _patient_.period_start
                        }
                    },
                ],
                "gender": _patient_.gender,
                "birthDate": _patient_.brthdate,
                "deceasedBoolean": false,
                "nationality": {
                    "coding": [
                        {
                            "system": "http://www.thcc.or.th/download/nationalitycode.xls",
                            "code": _patient_.nation,
                            "display": _patient_.nation_name
                        }
                    ],
                    "text": _patient_.nation_name
                },
                "address": [
                    {
                        "use": "home",
                        "type": "both",
                        "text": "ที่อยู่",
                        "line": [
                            _patient_.addrpart, _patient_.village_name
                        ],
                        "city": _patient_.subdistrict_name,
                        "district": _patient_.district_name,
                        "state": _patient_.province_name,
                        "postalCode": "",
                        "country": "TH",
                        "period": {
                            "start": _patient_.period_start
                        },
                        "address_code": _patient_.address_code
                    }
                ],
                "maritalStatus": {
                    "coding": [
                        {
                            "system": "http://terminology.hl7.org/CodeSystem/v3-MaritalStatus",
                            "code": _patient_.marital_status,
                            "display": _patient_.martial_status_name
                        }
                    ],
                    "text": _patient_.martial_status_name
                },
                "contact": [
                    {
                        "relationship": [
                            {
                                "coding": [
                                    {
                                        "system": "https://www.this.or.th",
                                        "code": "1",
                                        "display": _patient_.relation,
                                    }
                                ],
                                "text": _patient_.relation
                            }
                        ],
                        "name": [{
                            "use": "official",
                            "text": _patient_.contact_name,
                            "family": _patient_.contact_name,
                            "languageCode": "TH",
                            "given": [
                                ""
                            ],
                            "prefix": [
                                ""
                            ],
                            "suffix": [

                            ],
                            "period": {
                                "start": _patient_.period_start
                            }
                        }],
                        "telecom": [
                            {
                                "system": "phone",
                                "value": _patient_.contact_phone,
                                "use": "mobile",
                                "rank": "1",
                                "period": {
                                    "start": _patient_.period_start
                                }
                            }
                        ],
                        "address": [{
                            "use": "home",
                            "type": "both",
                            "text": "ที่อยู่",
                            "line": [
                                _patient_.contact_address
                            ],
                            "city": _patient_.subdistrict_name,
                            "district": _patient_.district_name,
                            "state": _patient_.province_name,
                            "postalCode": "",
                            "country": "TH",
                            "period": {
                                "start": _patient_.period_start
                            },
                            "address_code": _patient_.address_code
                        }],
                        "gender": _patient_.gender
                    }
                ]
            }
            let _allergyIntolerance: any = await fromImportModel.getAllergyIntolerance(db, req);
            let info_allergyIntolerance: any = _allergyIntolerance.rows;

            // let info_allergyIntolerance:any = [];
            let _allergyIntolerance_: any = []

            if (info_allergyIntolerance[0]) {
                for (const x of info_allergyIntolerance) {
                    let info = {
                        "managingOrganization": _managingOrganization,
                        "identifier":
                            [
                                {
                                    "system": "http://acme.com/ids/patients/risks",
                                    "value": x.hn
                                }
                            ],
                        "clinicalStatus": {
                            "coding":
                                [
                                    {
                                        "system":
                                            "http://terminology.hl7.org/CodeSystem/allergyintolerance-clinical",
                                        "code": "active",
                                        "display": "Active"
                                    }]
                        },
                        "verificationStatus": {
                            "coding":
                                [
                                    {
                                        "system":
                                            "http://terminology.hl7.org/CodeSystem/allergyintolerance-verification",
                                        "code": "confirmed",
                                        "display": "Confirmed"
                                    }
                                ]
                        },
                        "type": "allergy",
                        "category":
                            [
                                "medication"
                            ],
                        "criticality": "high",
                        "code": {
                            "coding":
                                [
                                    {
                                        "system": "https://www.this.or.th/tmt/substance",
                                        "code": x.tmt,
                                        "display": x.drug_name
                                    }
                                ]
                        },
                        "recordedDate": "2022-05-17",
                        "recordOfficer": {
                            "reference": "Officer/" + x.officer_code,
                            "identifier": x.officer_code,
                            "display": x.officer_name
                        },
                        "seriousness": {
                            "coding":
                                [
                                    {
                                        "system": "https://www.code.com/seriousness",
                                        "code": x.seriousness_code,
                                        "display": x.seriousness_name
                                    }
                                ]
                        },
                        "preventable": false,
                        "preventableScore": 0,
                        "reaction":
                            [
                                {
                                    "manifestation":
                                        [
                                            {
                                                "coding":
                                                    [
                                                        {
                                                            "system": "http://snomed.info/sct",
                                                            "code": "00000",
                                                            "display": x.reaction
                                                        }
                                                    ]
                                            }
                                        ]
                                }
                            ]
                    }

                    _allergyIntolerance_.push(info);
                }

            }

            let _coverage: any = await fromImportModel.getCoverage(db, req);
            let info_coverage: any = _coverage.rows;
            //let info_coverage:any = [];
            let _coverage_: any = []

            if (info_coverage[0]) {
                for (const x of info_coverage) {
                    let info = {
                        "identifier": [
                            {
                                "system": "https://www.nhso.go.th/certificate",
                                "value": x.card_id
                            },
                            {
                                "system": "https://www.nhso.go.th/authcode",
                                "value": ""
                            }
                        ],
                        "subscriberId": x.card_id,
                        "status": "active",
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ActCode",
                                    "code": "PUBLICPOL",
                                    "display": "public healthcare"
                                }
                            ]
                        },
                        "relationship": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/subscriber-relationship",
                                    "code": "self",
                                    "display": "Self"
                                }
                            ]
                        },
                        "period": {
                            "start": x.period_start,
                            "end": x.period_end
                        },
                        "payor": [
                            {
                                "reference": "สำนักงานหลักประกันสุขภาพแห่งชาติ"
                            }
                        ],
                        "class": [
                            {
                                "type": {
                                    "coding": [
                                        {
                                            "system": "http://terminology.hl7.org/CodeSystem/coverage-class",
                                            "code": "group"
                                        }
                                    ]
                                },
                                "value": x.group_value,
                                "name": x.group_name
                            },
                            {
                                "type": {
                                    "coding": [
                                        {
                                            "system": "http://terminology.hl7.org/CodeSystem/coverage-class",
                                            "code": "subgroup"
                                        }
                                    ]
                                },
                                "value": x.subgroup_value,
                                "name": x.subgroup_name
                            }
                        ],
                        "reimbursementAmount": x.amount,
                        "contract": [
                            {
                                "reference": "MainHospital",
                                "identifier": x.main_hospital,
                                "display": x.main_hospital_name
                            },
                            {
                                "reference": "SubHospital",
                                "identifier": x.sub_hospital,
                                "display": x.sub_hospital_name
                            }
                        ]
                    }
                    _coverage_.push(info);
                }
            }

            let _observation: any = await fromImportModel.getObservation(db, req);
            let info_observation: any = _observation.rows;
            // let info_observation:any = [];
            let _observation_: any = []

            if (info_observation[0]) {
                for (const x of info_observation) {
                    let valueString:any = null;
                    let valueQuantity:any = null;

                    if(parseFloat(x.lab_result) > 0){
                        valueQuantity = {
                            "value": parseFloat(x.lab_result),
                            "unit": x.unit || '-'    
                        }
                    }else{
                        valueString = x.lab_result
                    }

                    let info = {
                        "status": x.status,
                        "issued": x.issued,
                        "code": {
                            "coding": [
                                {
                                    "system": "https://fhir.loinc.org/CodeSystem/?url=http://loinc.org",
                                    "code": x.loinc_code || x.tmlt_code || '00000',
                                    "display": x.loinc_name || x.lab_name || '-'
                                },
                                {
                                    "system": "https://tmlt.this.or.th/tmlt/",
                                    "code": x.tmlt_code || '00000',
                                    "display": x.lab_name || '-'
                                }

                            ],
                            "text": x.lab_code_local
                        },
                        "valueQuantity": valueQuantity,
                        "profile_group": x.lab_name,
                        "valueString": valueString,
                        "referenceRange": [
                            {
                                "low": {
                                    "value": parseFloat(x.ref_min) || parseFloat(x.ref_max)
                                },
                                "high": {
                                    "value": parseFloat(x.ref_max)
                                },
                                "type": {
                                    "coding": [
                                        {
                                            "system": "http://terminology.hl7.org/CodeSystem/referencerangemeaning",
                                            "code": "normal",
                                            "display": "Normal Range"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                    if(!valueString){
                        delete info.valueString;
                    }
                    if(!valueQuantity){
                        delete info.valueQuantity
                    }

                    _observation_.push(info);


                }
            }


            let _condition: any = await fromImportModel.getCondition(db, req);
            let info_condition: any = _condition.rows;
            // let info_condition:any = [];
            let _condition_: any = []
            if (info_condition[0]) {
                for (const x of info_condition) {
                    let info = {
                        "clinicalStatus": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/conditionclinical",
                                    "code": "active",
                                    "display": "Active"
                                }
                            ]
                        },
                        "verificationStatus": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/condition-verstatus",
                                    "code": "confirmed",
                                    "display": "Confirmed"
                                }
                            ]
                        },
                        "category": [
                            {
                                "coding": [
                                    {
                                        "system": "http://snomed.info/sct",
                                        "code": "439401001",
                                        "display": "Diagnosis"
                                    }
                                ]
                            }
                        ],
                        "severity": {
                            "coding": [
                                {
                                    "system": "http://snomed.info/sct",

                                    "code": "24484000",
                                    "display": "Severe"
                                }
                            ]
                        },
                        "code": {
                            "coding": [
                                {
                                    "system": "http://hl7.org/fhir/sid/icd-10",
                                    "code": x.diag_code,
                                    "display": x.diag_name
                                }
                            ],
                            "text": x.diag_name
                        },
                        "bodySite": [
                            {
                                "coding": [
                                    {
                                        "system": "http://snomed.info/sct",
                                        "code": "",
                                        "display": ""
                                    }
                                ],
                                "text": ""
                            }
                        ],
                        "recordedDate": x.record_time
                    }
                    _condition_.push(info);
                }
            }

            let _vital_signs: any = await fromImportModel.getVital_signs(db, req);
            let info_vital_signs: any = _vital_signs.rows;

            // let info_vital_signs:any = [];
            let _vital_signs_: any = [];
            if (info_vital_signs[0]) {
                for (const x of info_condition) {

                    let info = {
                        "body_weight": {
                            "status": "final",
                            "valueQuantity": {
                                "value": info_vital_signs.bw,
                                "unit": "kg"
                            }
                        },
                        "body_height": {
                            "status": "final",
                            "valueQuantity": {
                                "value": info_vital_signs.height,
                                "unit": "cm"
                            }
                        },
                        "body_temp": {
                            "status": "final",
                            "valueQuantity": {
                                "value": info_vital_signs.tt,
                                "unit": "cel"
                            }
                        },
                        "bp_systolic": {
                            "status": "final",
                            "valueQuantity": {
                                "value": info_vital_signs.sbp,
                                "unit": "mmHg"
                            },
                            "interpretation": {
                                "text": info_vital_signs.sbp_interpretation
                            }
                        },
                        "bp_diastolic": {
                            "status": "final",
                            "valueQuantity": {
                                "value": info_vital_signs.dbp,
                                "unit": "mmHg"
                            },
                            "interpretation": {
                                "text": info_vital_signs.dbp_interpretation
                            }
                        }
                    }
                    _vital_signs_.push(info);
                }
            }

            let _medication: any = await fromImportModel.getMedication(db, req);
            let info_medication: any = _medication.rows;

            // console.log(info_medication);
            // let info_medication: any = [];
            let _medication_: any = [];
            if (info_medication[0]) {
                for (const x of info_medication) {
                    let info = {
                        "code": {
                            "coding": [
                                {
                                    "system": "https://www.this.or.th/tmt/gp",
                                    "code": x.tmt_code || '00000',
                                    "display": x.generic_name || '-'
                                }
                            ],
                            "text": x.drug_name  || '-'
                        },
                        "form": {
                            "coding": [
                                {
                                    "system": "http://snomed.info/sct",
                                    "code": "732937005",
                                    "display": x.unit
                                }
                            ]
                        },
                        "finance": {
                            "qty": parseFloat(x.qty),
                            "unitPrice": parseFloat(x.price)
                        },
                        "statement": {
                            "status": "active",
                            "category": {
                                "coding": [
                                    {
                                        "system": "http://terminology.hl7.org/CodeSystem/medication-statement-category",
                                        "code": x.category,
                                        "display": x.category_display
                                    }
                                ]
                            },
                            "effectiveDateTime": x.prsc_time,
                            "note": [
                                {
                                    "time": x.prsc_time,
                                    "text": "ทดสอบ Note"
                                }
                            ],
                            "dosage": [
                                {
                                    "sequence": 1,
                                    "text": x.use_code,
                                    "patientInstruction": x.med_use,
                                    "timing": {
                                        "repeat": {
                                            "frequency": 3,
                                            "period": 1,
                                            "periodUnit": "d"
                                        }
                                    },
                                    "route": {
                                        "coding": [
                                            {
                                                "system": "http://standardterms.edqm.eu",
                                                "code": "20053000",
                                                "display": x.use_route
                                            }
                                        ]
                                    },
                                    "doseAndRate": [
                                        {
                                            "type": {
                                                "coding": [
                                                    {
                                                        "system": "http://terminology.hl7.org/CodeSystem/dose-rate-type",
                                                        "code": "ordered",
                                                        "display": "Ordered"
                                                    }
                                                ]
                                            },
                                            "doseQuantity": {
                                                "value": 1,
                                                "unit": "tablet",
                                                "system": "http://http://snomed.info/sct",
                                                "code": "732936001"
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                    _medication_.push(info);
                }
            }

            let _appointment: any = await fromImportModel.getAppointment(db, req);        
            let info_appointment:any = _appointment.rows;
            // console.log(info_appointment);
            
            // let info_appointment: any = [];
            let _appointment_: any = [];
            if (info_appointment[0]) {
                for(const x of info_appointment){
                    let info = {
                        "status": "booked",
                        "serviceCategory": [
                            {
                                "coding": [
                                    {
                                        "system": "http://example.org/service-category",
                                        "code": "",
                                        "display": ""
                                    }
                                ]
                            }
                        ],
                        "serviceType": [
                            {
                                "coding": [
                                    {
                                        "code": x.clinic,
                                        "display": x.location
                                    }
                                ]
                            }
                        ],
                        "specialty": [{
                            "coding": [
                                {
                                    "system": "http://snomed.info/sct",
                                    "code": "",
                                    "display": ""
                                }
                            ]
                        }
                        ],
                        "appointmentType": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v2-0276",
                                    "code": "",
                                    "display": ""
                                }
                            ]
                        },
                        "reason": [
                            {
                                "reference": {
                                    "reference": x.clinic,
                                    "display": x.location
                                }
                            }
                        ],
                        "description": x.follow_detail,
                        "start": x.fudate,
                        "end":x.fudate,
                        "created": x.register_date,
                        "note": [
                            {
                                "text": x.follow_detail
                            }
                        ],
                        "patientInstruction": [
                            {
                                "concept": {
                                    "text": x.patient_instruction || x.location || '-'
                                }
                            }
                        ],
                        "basedOn": [
                            {
                                "reference": "ServiceRequest/myringotomy"
                            }
                        ],
                        "subject": {
                            "reference": "Patient/example",
                            "display": x.ptname
                        },
                        "participant": [
                            {
                                "actor": {
                                    "reference": "Patient/example",
                                    "display": x.ptname
                                },
                                "required": true,
                                "status": "accepted"
                            },
                            {
                                "type": [
                                    {
                                        "coding": [
                                            {
                                                "system": "http://terminology.hl7.org/CodeSystem/v3-ParticipationType",
                                                "code": "ATND"
                                            }
                                        ]
                                    }
                                ],
                                "actor": {
                                    "reference": "Practitioner/example",
                                    "display": x.participant
                                },
                                "required": true,
                                "status": "accepted"
                            },
                            {
                                "actor": {
                                    "reference": x.clinic,
                                    "display": x.location
                                },
                                "required": true,
                                "status": "accepted"
                            }
                        ]

                    }
                _appointment_.push(info);
                }
            }

            let _immunization: any = await fromImportModel.getImmunization(db, req);        
            let info_immunization:any = _immunization.rows;
            // let info_immunization: any = [];
            let _immunization_: any = [];
            if (info_immunization[0]) {
                for(const x of info_immunization){
                    let info = {
                        "status": "completed",
                        "vaccineCode": {
                            "coding": [
                                {
                                    "system": "http://www.thcc.or.th/download/epi.xls",
                                    "code": x.vaccine_code
                                }
                            ],
                            "text": x.vaccine_name
                        },
                        "occurrenceDateTime": x.date_serv,
                        "primarySource": true,
                        "location": x.vaccine_location,
                        "manufacturer": "",
                        "lotNumber": x.lot_no,
                        "expirationDate": x.expire_date,
                        "site": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-ActSite",
                                    "code": x.site_code,
                                    "display": x.site_name
                                }
                            ]
                        },
                        "route": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/v3-RouteOfAdministration",
                                    "code": x.vaccine_route,
                                    "display": x.route_name
                                }
                            ]
                        },
                        "doseQuantity": {
                            "value": 5,
                            "system": "http://unitsofmeasure.org",
                            "code": "mg"
                        },
                        "note": [
                            {
                                "text": "ระวังอาการมีไข้"
                            }
                        ],
                        "reasonCode": [
                            {
                                "coding": [
                                    {
                                        "system": "http://snomed.info/sct",
                                        "code": "429060002"
                                    }
                                ]
                            }
                        ],
                        "performer": {
                            "license_no": x.reference_code + x.participant_reference,
                            "name": x.participant
                        }
    
    
                    }
                    _immunization_.push(info);
    
                }

            }

            let _claim: any = await fromImportModel.getClaim(db, req);        
            let info_claim:any = _claim.rows;
            // let info_claim: any = [];
            let _claim_: any = [];
            if (info_claim[0]) {
                for(const x of info_claim){
                    let info = {
                        "identifier": [
                            {
                                "system": "http://happyvalley.com/claim",
                                "value": x.claim_no
                            }
                        ],
                        "status": "active",
                        "type": {
                            "coding": [
                                {
                                    "system": "http://terminology.hl7.org/CodeSystem/claim-type",
                                    "code": "professional"
                                }
                            ]
                        },
                        "use": "claim",
                        "careTeam": [
                            {
                                "sequence": 1,
                                "provider": {
                                    "reference": "Practitioner/"+ x.reference_code + x.participant_reference,
                                    "identifier": x.reference_code + x.participant_reference,
                                    "display": x.participant
                                }
                            },
                        ],
                        "item": [
                            {
                                "sequence": 1,
                                "careTeamSequence": [
                                    1
                                ],
                                "productOrService": {
                                    "coding": [
                                        {
                                            "code": x.claim_type
                                        }
                                    ]
                                },
                                "servicedDate": x.date_serv,
                                "unitPrice": {
                                    "value": x.cost,
                                    "currency": "THB"
                                },
                                "net": {
                                    "value": x.price,
                                    "currency": "THB"
                                },
                                "detail": [
                                    {
                                        "sequence": 1,
                                        "productOrService": {
                                            "coding": [
                                                {
                                                    "system": "http://example.org/fhir/oralservicecodes",
                                                    "code": x.claim_type_name
                                                }
                                            ]
                                        },
                                        "unitPrice": {
                                            "value": x.cost,
                                            "currency": "THB"
                                        },
                                        "net": {
                                            "value": x.price,
                                            "currency": "THB"
                                        }
                                    }]
                            }]
                    }
                    _claim_.push(info);    
                }

            }


            let _encounter: any = await fromImportModel.getEncounter(db, req);
            let info_encounter: any = _encounter.rows[0];
            // let info_encounter:any = [];
            let _encounter_: any;
            if (info_encounter) {
                let info = {
                    "managingOrganization": _managingOrganization,
                    "identifier": [
                        {
                            "use": "official",
                            "system": "https://bps.moph.go.th/vn",
                            "value": info_encounter.vn.toString()
                        },
                        {
                            "use": "official",
                            "system": "https://sil-th.org/hn",
                            "value": info_encounter.hn.toString(),
                            "period": {
                                "start": info_encounter.start_period.toString()
                            }
                        }
                    ],
                    "status": "finished",
                    "class": {
                        "system": "https://terminology.hl7.org/CodeSystem/v3-ActCode",
                        "code": info_encounter.class,
                        "display": info_encounter.class_name
                    },
                    "subclass": {
                        "system": "https://bps.moph.go.th/subclass",
                        "code": info_encounter.subclass,
                        "display": info_encounter.subclass_name
                    },
                    "division": {
                        "system": "https://bps.moph.go.th/division",
                        "code": info_encounter.division,
                        "display": info_encounter.division_name
                    },
                    "type": {
                        "coding": [
                            {
                                "system": "https://spd.moph.go.th/new_bps/43file_version2.3",
                                "code": info_encounter.type_code,
                                "display": info_encounter.type_name
                            }
                        ],
                        "text": "มารับบริการเอง"
                    },
                    "priority": {
                        "coding": [
                            {
                                "system": "http://terminology.hl7.org/CodeSystem/v3-ActPriority",
                                "code": info_encounter.piority_code,
                                "display": info_encounter.piority_name
                            }
                        ],
                        "text": info_encounter.piority_text
                    },
                    "period": {
                        "start": info_encounter.period_start,
                        "end": info_encounter.period_end
                    },
                    "subject": {
                        "reference": "Patient/" + info_encounter.hn,
                        "display": info_encounter.ptname
                    },
                    "screen_allergy": {
                        "system": "https://bps.moph.go.th/screen_allergy",
                        "code": info_encounter.screen_allergy_code,
                        "display": info_encounter.screen_allergy
                    },
                    "screen_smoking": {
                        "system": "https://bps.moph.go.th/screen_smoking",
                        "code": info_encounter.screen_smoke_code,
                        "display": info_encounter.screen_smoke
                    },
                    "screen_drinking": {
                        "system": "https://bps.moph.go.th/screen_smoking",
                        "code": info_encounter.screen_drink_code,
                        "display": info_encounter.screen_drink
                    },
                    "participant": [
                        {
                            "individual": {
                                "type": {
                                    "text": info_encounter.participant_type
                                },
                                "reference": info_encounter.reference_code + info_encounter.participant_reference,
                                "display": info_encounter.participant
                            }
                        }
                    ],
                    "reason": [
                        {
                            "text": info_encounter.reason
                        }
                    ],
                    "financeTotalAmount": parseFloat(info_encounter.total_amount),
                    "financeReimbursementAmount": info_encounter.total_amount - info_encounter.paid_amount,
                    "financePaidAmount": parseFloat(info_encounter.paid_amount),
                    "Coverage": _coverage_,
                    "vital_signs": _vital_signs_,
                    "Observation": _observation_,
                    "Condition": _condition_,
                    "Medication": _medication_,
                    "Appointment": _appointment_,
                    "Immunization": _immunization_,
                    "Claim": _claim_
                }
                _encounter_ = info;
            }

            rs = {
                "managingOrganization": _managingOrganization,
                "Patient": _patient,
                "AllergyIntolerance": _allergyIntolerance_,
                "Encounter": [_encounter_]
            }

            let info_visit: any;

            if (token) {
                info_visit = await getPhrModel.insert_phr(token, rs);
            } else {
                info_visit = {};
            }

            // console.log(rs);
            reply.code(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, results: rs, results_phr: info_visit });
        } catch (error: any) {
            console.log(error);

            console.log(HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR));
            reply.code(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).send({
                statusCode: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
                results: {
                    ok: false,
                    text: "การอ่านข้อมูลเกิดความผิดพลาด",
                    error: HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR),
                    error_text: error.toString()
                }
            });
        }
    });

    //list phr log
    fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        try {
          var rs = await fromImportModel.list(db, req);
          // console.log(rs);
          reply.code(200).send(rs);
        } catch (error: any) {
          req.log.error(error);
          reply.code(500).send({
            ok: false,
            text: "การอ่านข้อมูลเกิดความผิดพลาด",
            error: error.message
          });
        }
      });

    //Save phr log
    fastify.post('/', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const info = req.body;
    
        try {
          var rs = await fromImportModel.save(db, info);
          reply.code(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, results: rs });
        } catch (error: any) {
          req.log.error(error);
          reply.code(500).send({
            ok: false,
            text: "การเพิ่มข้อมูลเกิดความผิดพลาด",
            error: error.message
          });
        }
      });
    
      //   UPDATE phr log
      fastify.put('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request;
        const info = req.body;
        const id = req.params.id;
        try {
          var rs = await fromImportModel.update(db, id, info);
          reply.code(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, results: rs });
        } catch (error: any) {
          var rs = await fromImportModel.select_id(db, id);
          console.log(HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR));
          reply.code(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).send({
            statusCode: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR,
            results:{
                ok: false,
                text: "การอ่านข้อมูลเกิดความผิดพลาด",
                error: HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR)
            }
          });    
        }
      });
    
}