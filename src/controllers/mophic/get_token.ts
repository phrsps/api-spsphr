import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import * as HttpStatus from 'http-status-codes';

import { GetTokenModel } from '../../models/mophic/get_token'

const fromImportModel = new GetTokenModel();

export default async (fastify: FastifyInstance) => {
  const db = fastify.db;
  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(HttpStatus.StatusCodes.OK).send({ message: 'ExportVaccineModel  -->Fastify, RESTful API services! HI720220220' });
  });


  fastify.get('/getToken', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;

    const user = req.query.user
    const password_hash = req.query.password_hash
    const hospital_code = req.query.hospital_code
try {
    const rs: any = await fromImportModel.select(user,password_hash,hospital_code);
    reply.status(HttpStatus.StatusCodes.OK).send({ statusCode: HttpStatus.StatusCodes.OK, info: rs });
  } catch (error:any) {
    // fastify.log.error(error);
    reply.status(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR).send({ statusCode: HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR, message: HttpStatus.getStatusText(HttpStatus.StatusCodes.INTERNAL_SERVER_ERROR) })
  }

});
}