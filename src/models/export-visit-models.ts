import { Knex } from 'knex'

export class ExportVisitModel {

    async viewVisit(dbHIS: Knex, dateStart: any, dateEnd: any, hn: any) {
      // let dateCurrent = new Date();
      let sql:any;
      if(hn && !dateStart && !dateEnd){
        console.log('1');
        
        sql = `
        Select 
        date_format(o.vstdttm,'%Y-%m-%dT%H:%i:%s.000Z') as vstdttm,
        o.vn,
        o.hn,
        concat(p.fname,' ',p.lname) as ptname,
        p.pop_id as cid,
        c.namecln as clinic
        From 
        hi.ovst as o 
        inner join 
        hi.pt as p on o.hn=p.hn
        left join 
        hi.cln as c on o.cln=c.cln 
        where o.hn = '${hn}' 
        order by o.vn desc
        `;
        return await dbHIS.raw(sql)

      }

      if(!hn &&dateStart && dateEnd){
        console.log('2');

        sql = `
        Select 
        date_format(o.vstdttm,'%Y-%m-%dT%H:%i:%s.000Z') as vstdttm,
        o.vn,
        o.hn,
        concat(p.fname,' ',p.lname) as ptname,
        p.pop_id as cid,
        c.namecln as clinic
        From 
        hi.ovst as o 
        inner join 
        hi.pt as p on o.hn=p.hn
        left join 
        hi.cln as c on o.cln=c.cln 
        where date(o.vstdttm) between '${dateStart}' and '${dateEnd}'
        order by o.vn desc
        `;
        return await dbHIS.raw(sql)

      }

      if(hn && dateStart && dateEnd){
        console.log('3');

        sql = `
        Select 
        date_format(o.vstdttm,'%Y-%m-%dT%H:%i:%s.000Z') as vstdttm,
        o.vn,
        o.hn,
        concat(p.fname,' ',p.lname) as ptname,
        p.pop_id as cid,
        c.namecln as clinic
        From 
        hi.ovst as o 
        inner join 
        hi.pt as p on o.hn=p.hn
        left join 
        hi.cln as c on o.cln=c.cln 
        where o.hn = '${hn}' and date(o.vstdttm) between '${dateStart}' and '${dateEnd}'
        order by o.vn desc
        `;
        return await dbHIS.raw(sql)

      }
      if(!hn && !dateStart && !dateEnd){
        return [{ok:false,text:'เงื่อนไขไม่ถูกต้อง'}];
      }else{
        return [{ok:false,text:'เงื่อนไขไม่ถูกต้อง'}];
      }
    }
  
  }