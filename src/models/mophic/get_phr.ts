import { Knex } from 'knex'
const request = require("request");

export class GetPhrModel {
    async insert_phr(token: any, info: any) {        
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `https://phr1.moph.go.th/api/UpdatePHRv1`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: info
            ,
            json: true
          };
    
          request(options, function (error: any, response: any, body: any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }

}