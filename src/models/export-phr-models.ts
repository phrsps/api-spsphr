import { Knex } from 'knex'

export class ExportPhrModel {

    async getManagingOrganization(db: Knex) {

        let data: any = await db.raw(`
        select 
            hcode as hospcode
            ,hi_hsp_nm as hospname
            ,hi_chw_cd as province_code
            ,hi_chw_nm as province_name
            ,hi_amp_cd as district_code
            ,hi_hsp_cd as address_code
        from hi.setup limit 1
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getPatient(db: Knex, req: any) {
        // console.log(req);
        let info:any = req.body;
        let data: any = await db.raw(`
            select 
                p.pop_id as cid
                ,p.hn
                ,fname
                ,lname
                ,if(pname <> '',pname,f.prename) as prename
                ,if(engpname <> '',concat(engpname,'.'),engpname) as pname_eng
                ,engfname
                ,englname
                ,if(engfname <> '',concat(if(engpname <> '',concat(engpname,'.'),engpname),engfname,' ',englname),'') as fullname_eng
                ,hometel
                ,if(substr(hometel,0,2) in ('08','09'),'mobile','home') as phone_type
                ,if(p.male = '1','male','female') as gender
                ,p.brthdate
                ,p.ntnlty as nation
                ,n.namentnlty as nation_name
                ,c.namechw as province_name
                ,a.nameampur as district_name
                ,t.nametumb as subdistrict_name
                ,m.namemoob as village_name
                ,p.addrpart
                ,concat(p.chwpart,p.amppart,p.tmbpart) as address_code
                ,(case p.mrtlst 
                    when '1' then 'S' 
                    when '2' then 'M' 
                    when '3' then 'W' 
                    when '4' then 'D' 
                    when '5' then 'A' 
                    when '6' then 'U'
                    else 'S' end
                ) as marital_status
                ,s.namemrt as martial_status_name
                ,p.infmname as contact_name
                ,p.statusinfo as relation
                ,p.infmtel as contact_phone
                ,p.infmaddr as contact_address
                ,date_format(p.fdate,'%Y-%m-%dT%H:%i:%s.000Z') as period_start
                ,year(fdate) as visit_start
                ,year(brthdate) as cid_start
            from 
            hi.pt as p
            left join hi.ntnlty as n on p.ntnlty=n.ntnlty
            left join hi.l_prename as f on (CASE p.male 
                WHEN 1 THEN 
                IF( p.mrtlst < 6, IF( TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '001', '003' ),
                IF(  TIMESTAMPDIFF(year,p.brthdate,NOW()) < 20, '823', '831' )) 
                WHEN 2 THEN 
                IF( p.mrtlst = 1,IF(  TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '002', '004' ),
                IF ( p.mrtlst < 6, '005', '863' )) END) = f.prename_code
            left join hi.changwat as c on p.chwpart = c.chwpart
            left join hi.ampur as a on p.chwpart = a.chwpart and p.amppart = a.amppart
            left join hi.tumbon as t on p.chwpart = t.chwpart and p.amppart = t.amppart and p.tmbpart = t.tmbpart
            left join hi.mooban as m on p.chwpart = m.chwpart and p.amppart = m.amppart and p.tmbpart = m.tmbpart and p.moopart = m.moopart
            left join hi.mrtlst as s on p.mrtlst =s.mrtlst
            where p.hn = '${info.hn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล patient',
            rows: data[0]
        }
        return result;
    }

    async getAllergyIntolerance(db: Knex, req: any) {
        let info:any = req.body;
        
        let data: any = await db.raw(`
        SELECT
        a.hn
        ,ifnull(m.name,a.namedrug) as drug_name
        ,m.tmtid as tmt
        ,l.namealevel as seriousness_name
        ,t.nameallgtype as preven
        ,a.alevel as criticaly
        ,a.allgtype as seriousness_code
        ,a.detail as reaction
        ,a.pharmacist officer_code
        ,concat(r.pname,r.fname,' ',r.lname) as officer_name
        FROM
        hi.allergy as a
        left join 
        hi.alevel as l on a.alevel=l.codealevel
        left join 
        hi.allgtype as t on a.allgtype = t.allgtype
        left join 
        hi.meditem as m on a.meditem=m.meditem
        left join 
        hi.phrmcst as r on a.pharmacist=r.pmc
        where a.hn = '${info.hn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getCarePlan(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(``);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getEncounter(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        Select 
        o.vn
        ,o.hn
        ,concat(if(p.pname <> '',p.pname,f.prename),p.fname,' ',p.lname) as ptname
        ,year(p.fdate) as start_period
        ,if(p.allergy = '','ไม่ทราบ/ไม่สามารถซักประวัติได้',if(p.allergy like 'ไม่%' or p.allergy like 'ปฏิเสธ%','ปฏิเสธแพ้ยา','แพ้ยา')) as screen_allergy
        ,if(p.allergy = '','9',if(p.allergy like 'ไม่%' or p.allergy like 'ปฏิเสธ%','1','2')) as screen_allergy_code
        ,(case 
            when o.an = 0 and t.ucae in ('A','E') then 'EMER'
            when o.an = 0 and t.ucae not in ('A','E') then 'AMB'
            when o.an = 0 and s.service = '2' then 'HH'
            when o.an > 0 and t.ucae not in ('A','E') then 'IMP'
            when o.an > 0 and t.ucae in ('A','E') then 'ACUTE'
            else 'AMB'
            end ) as 'class'
        ,(case 
            when o.an = 0 and t.ucae in ('A','E') then 'emergency'
            when o.an = 0 and t.ucae not in ('A','E') then 'ambulatory'
            when o.an = 0 and s.service = '2' then 'Home Healthcare'
            when o.an > 0 and t.ucae not in ('A','E') then 'Inpatient'
            when o.an > 0 and t.ucae in ('A','E') then 'Inpatient acute'
            else 'ambulatory'
            end ) as 'class_name'
        ,if(o.an = 0 ,o.cln,i.ward) as subclass
        ,if(o.an = 0 ,c.namecln,w.dspname) as subclass_name
        ,if(o.an = 0 ,substr(o.cln,2,2),i.ward) as division
        ,if(o.an = 0 ,sp.namespclty,d.nametype) as division_name
        ,if(ri.rfrlct is null,'1','3') as type_code
        ,if(ri.rfrlct is null,'มารับบริการเอง','ส่งต่อจากหน่วยบริการอื่น') as type_name
        ,(case t.triage 
            when 1 then 'S'
            when 2 then 'EM'
            when 3 then 'UR'
            when 4 then 'PRN'
            else 'R' end) as piority_code
        ,(case t.triage 
            when 1 then 'stat'
            when 2 then 'emergency'
            when 3 then 'urgent'
            when 4 then 'as needed'
            else 'routine' end) as piority_name
        ,(case t.triage 
            when 1 then 'จำเป็นต้องรักษาทันที'
            when 2 then 'ฉุกเฉิน'
            when 3 then 'เร่งด่วน'
            when 4 then 'ตามความจำเป็น'
            else 'ไม่เร่งด่วน' end) as piority_text
        ,if(o.smoke = '0','1','2') as screen_smoke_code
        ,if(o.drink = '0','1','2') as screen_drink_code
        ,if(o.smoke = '0','ไม่สูบบุหรี่','สูบบุหรี่') as screen_smoke
        ,if(o.drink = '0','ไม่ดื่มสุรา','ดื่มสุรา') as screen_drink
        ,if(o.cln = '40100',concat(p_dt.prename,dentist.fname,' ',dentist.lname),concat(p_dr.prename,dct.fname,' ',dct.lname)) as participant
        ,if(o.cln = '40100',dentist.lcno,dct.lcno) as participant_reference
        ,if(o.cln = '40100',if(dentist.lcno <> '','ท.',''),if(dct.council in ('01','011'),'ว.','')) as reference_code
        ,if(o.cln = '40100',lcd.council_name,lc.council_name) as participant_type
        ,cc.symptom as reason
        ,ifnull((select sum(incoth.rcptamt) from hi.incoth where incoth.vn=o.vn),0) as total_amount
        ,ifnull((select sum(rcpt.amnt) from hi.rcpt where rcpt.vn=o.vn),0) as paid_amount
        ,date_format(o.vstdttm,'%Y-%m-%dT%H:%i:%s.000Z') as period_start
        ,if(o.an = 0,if(pr.prscno is null,concat(date(o.vstdttm),'T',time(o.drxtime*100),'.000Z'),concat(pr.prscdate,'T',time(pr.prsctime*100),'.000Z')),concat(i.dchdate,'T',time(i.dchtime),'.000Z')) as period_end
    From 
    hi.ovst as o 
    inner join hi.pt as p on o.hn=p.hn 
    left join hi.l_prename as f on (CASE p.male 
        WHEN 1 THEN 
        IF( p.mrtlst < 6, IF( TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '001', '003' ),
        IF(  TIMESTAMPDIFF(year,p.brthdate,NOW()) < 20, '823', '831' )) 
        WHEN 2 THEN 
        IF( p.mrtlst = 1,IF(  TIMESTAMPDIFF(year,p.brthdate,NOW() ) < 15, '002', '004' ),
        IF ( p.mrtlst < 6, '005', '863' )) END) = f.prename_code
    left join hi.optriage as t on o.vn=t.vn
    left join hi.service_type as s on o.vn = s.vn
    left join hi.cln as c on o.cln = c.cln 
    left join hi.ipt as i on o.an=i.an
    left join hi.idpm as w on i.ward=w.idpm
    left join hi.spclty as sp on c.specialty=sp.spclty
    left join hi.l_rfrtype as d on i.dept = d.dept
    left join hi.orfri as ri on o.vn=ri.vn
    left join hi.prsc as pr on o.vn=pr.vn
    left join hi.dct on (
        CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
        WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
        WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
    left join hi.dt on o.vn=dt.vn
    left join hi.dentist on dt.dnt = dentist.codedtt
    left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
    left join hi.l_prename as p_dt on dentist.pname= p_dt.prename_code
    left join hi.l_council as lc on dct.providertype =lc.council
    left join hi.l_council as lcd on dentist.providertype =lcd.council
    left join hi.symptm as cc on o.vn=cc.vn
    left join hi.pttype on o.pttype=pttype.pttype
    where o.vn = '${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getCoverage(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        SELECT
        s.card_id
        ,date_format(s.datein,'%Y-%m-%d') as period_start
        ,date_format(s.dateexp,'%Y-%m-%d') as period_end
        ,t.inscl as group_value
        ,(case t.inscl when 'UCS' then 'สิทธิหลักประกันสุขภาพแห่งชาติ'
            when 'OFC' then 'สิทธิข้าราชการ'
            when 'LGO' then 'สิทธิองค์การปกครองส่วนท้องถิ่น'
            when 'SSS' then 'สิทธิประกันสังคม'
            when 'SSI' then 'สิทธิ์ประกันสังคมทุพพลภาพ'
        else 'อื่นๆ' end
        ) as group_name
        ,if(t.inscl = 'UCS',t.instypeold,t.stdcode) as subgroup_value
        ,if(t.inscl = 'UCS',t.namepttype,'') as subgroup_name
        ,s.hospmain as main_hospital
        ,h.hosname as main_hospital_name
        ,s.hospsub as sub_hospital
        ,h2.hosname as sub_hospital_name
        ,ifnull((select sum(incoth.rcptamt) from hi.incoth where incoth.vn=o.vn),0) - ifnull((select sum(rcpt.amnt) from hi.rcpt where rcpt.vn=o.vn),0) as amount
        FROM
        hi.ovst as o 
        inner join hi.insure as s on o.hn=s.hn and o.pttype=s.pttype
        inner join hi.pttype as t on o.pttype =t.pttype
        left join chospital as h on s.hospmain = h.hoscode
        left join chospital as h2 on s.hospsub = h2.hoscode
        where o.vn = '${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getVital_signs(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        Select 
        o.bw
        ,o.sbp
        ,if(o.sbp between 60 and 120,'normal',if(o.sbp > 120,'High','Low')) as sbp_interpretation
        ,o.dbp
        ,if(o.dbp between 40 and 90,'normal',if(o.sbp > 90,'High','Low')) as dbp_interpretation
        ,o.rr
        ,o.pr
        ,o.tt
        ,o.height
        from hi.ovst as o where o.vn='${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }


    async getObservation(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        select 
        l.ln
        ,l.labcode
        ,r.lab_name
        ,r.lab_code_local
        ,RTRIM(LTRIM(SUBSTRING_INDEX(r.labresult,'[',1))) as lab_result
        ,r.unit
        ,r.normal
        ,r.comment
        ,l.labcomment
        ,concat(l.srvdate,'T',time(l.srvtime*100),'.000Z') as issued
        ,if(l.finish = 1 , 'final','no result') as status
        ,lab.tmlt as tmlt_code
        ,lab.loinc as loinc_code
        ,lab.loincname as loinc_name
        ,replace(replace(replace(replace(SUBSTRING_INDEX(r.normal,'-',1),'>',''),'<',''),'ชาย',''),'หญิง','') as ref_min
        ,REplace(REPLACE(r.normal,SUBSTRING_INDEX(r.normal,'-',1),''),'-','') as ref_max
        from hi.lbbk as l 
        inner join lab on l.labcode=lab.labcode
        left join hi.labresult as r on l.ln=r.ln
        where l.vn = '${info.vn}'
        order by l.ln
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getCondition(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        select 
        if(length(x.icd10) > 3,concat(substr(x.icd10,1,3),'.',substr(x.icd10,4,4)),x.icd10) as diag_code
        ,c.icd10name as diag_name
        ,concat(date_format(o.vstdttm,'%Y-%m-%dT'),time(drxtime*100),'.000Z') as record_time
        from hi.ovstdx as x 
        inner join hi.ovst as o on o.vn=x.vn
        left join hi.icd101 as c on x.icd10=c.icd10 
        where x.vn = '${info.vn}'
        union 
        select 
        dtdx.icdda as diag_code
        ,icdda.nameicdda as diag_name
        ,date_format(dt.vstdttm,'%Y-%m-%dT%H:%i:%s.000Z') as record_time
        from 
        hi.dt 
        inner join hi.dtdx on dt.dn=dtdx.dn
        left join hi.icdda on dtdx.icdda=icdda.codeicdda
        where dt.vn = '${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getMedication(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        select
        if(o.an = 0,'outpatient','inpatient') as category
        ,if(o.an = 0,'ผู้ป่วยนอก','ผู้ป่วยใน') as category_display
        ,concat(p.prscdate,'T',time(p.prsctime*100),'.000Z') as prsc_time
        ,d.nameprscdt as drug_name
        ,i.name as generic_name
        ,i.tmtid as tmt_code
        ,if(m.dosename is null,x.doseprn,concat(m.doseprn1,m.doseprn2)) as med_use
        ,if(d.medusage in ('IM','IV'),'Injected','Oral use') as use_route
        ,d.medusage as use_code
        ,d.qty
        ,i.price as unit
        ,i.pres_unt
        from hi.ovst as o
        inner join hi.prsc as p on o.vn=p.vn
        inner join hi.prscdt as d on p.prscno=d.prscno 
        inner join hi.meditem as i on d.meditem=i.meditem and i.type in (1,3)
        left join hi.medusage as m on d.medusage=m.dosecode
        left join hi.xdose as x on d.xdoseno=x.xdoseno
        where o.vn='${info.vn}'
        order by d.id       
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getClaim(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        SELECT 
        o.vn
        ,concat('OP',o.vn) as claim_no
        ,'ค่าบริการผู้ป่วยนอก' as claim_type
        ,DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv
        ,i.income as claim_item_code
        ,im.namecost as claim_type_name
        ,REPLACE(format(SUM(IF( i.rcptamt IS NULL, 0.00, i.rcptamt )),2),',','') as cost
        ,REPLACE(format(SUM(IF( i.rcptamt IS NULL, 0.00, i.rcptamt )),2),',','') as price
        ,format(ifnull(sum(case when t.op56 > '5000' then i.rcptamt when t.instypeold = '89' and i.income='21' then 30 else 0 end),0.00),2) as payprice
        ,concat(p_dr.prename,dct.fname,' ',dct.lname) as participant
        ,dct.lcno as participant_reference
        ,if(dct.council in ('01','011'),'ว.','') as reference_code
        from hi.ovst as o 
        inner join hi.pt on o.hn=pt.hn
        inner join hi.incoth i on o.vn = i.vn  
        inner join hi.income im on i.income = im.costcenter 
        LEFT OUTER JOIN hi.pttype t on o.pttype = t.pttype 
        left outer join hi.cln c on o.cln = c.cln  
        left join hi.dct on (
        CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
        WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,3,2)  
        END )
        left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
        left join hi.l_council as lc on dct.providertype =lc.council
        where 
        o.vn = '${info.vn}'and o.an = 0 and i.income !='19' 
        group by 
        o.vn,i.income
        union
        SELECT 
        o.vn
        ,concat('OP',o.vn) as claim_no
        ,'ค่าบริการผู้ป่วยนอก' as claim_type
        ,DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv
        ,i.income as chargeitem
        ,im.namecost as claim_type_name
        ,REPLACE(format(SUM(IF( x.charge IS NULL, 0.00, x.charge )),2),',','') as cost
        ,REPLACE(format(SUM(IF( x.charge IS NULL, 0.00, x.charge )),2),',','') as price
        ,format(ifnull(sum(case when t.op56 > '5000' then x.charge when t.instypeold = '89' and i.income='21' then 30 else 0 end),0.00),2) as payprice
        ,concat(p_dt.prename,dentist.fname,' ',dentist.lname) as participant
        ,dentist.lcno as participant_reference
        ,if(dentist.lcno <> '','ท.','') as reference_code
        from hi.dt as o 
        inner join hi.dtdx as x on o.dn=x.dn
        inner join hi.pt on o.hn=pt.hn
        inner join hi.incoth i on o.vn = i.vn  
        inner join hi.income im on i.income = im.costcenter 
        LEFT OUTER JOIN hi.pttype t on o.pttype = t.pttype 
        left join hi.dentist on o.dnt = dentist.codedtt
        left join hi.l_prename as p_dt on dentist.pname= p_dt.prename_code
        left join hi.l_council as lcd on dentist.providertype =lcd.council
        where 
        o.vn = '${info.vn}' and o.an = 0 and i.income ='19' 
        group by 
        o.vn,i.income
        union 
        SELECT 
        o.vn
        ,concat('AN',i.an) as claim_no
        ,'ค่าบริการผู้ป่วยใน' as claim_type
        ,date_format(i.rgtdate,'%Y-%m-%d') as datetime_admit
        ,ic.income as claim_item_code
        ,im.namecost as claim_type_name
        ,REPLACE(format(SUM(IF( ic.rcptamt IS NULL, 0.00, ic.rcptamt )),2),',','') as cost
        ,REPLACE(format(SUM(IF( ic.rcptamt IS NULL, 0.00, ic.rcptamt )),2),',','') as price
        ,replace(format(ifnull(sum(case when p.op56 > '5000' then ic.rcptamt when p.instypeold = '89' and ic.income='21' then 30 else 0 end),0.00),2),',','') as payprice
        ,concat(p_dr.prename,dct.fname,' ',dct.lname) as participant
        ,dct.lcno as participant_reference
        ,if(dct.council in ('01','011'),'ว.','') as reference_code
        from hi.ipt i 
        inner join hi.pt on i.hn=pt.hn
        inner join hi.ovst as o on i.an=o.an
        inner join hi.incoth ic on o.vn = ic.vn  
        left outer join hi.idpm on i.ward = idpm.idpm 
        left outer join hi.income im on ic.income = im.costcenter  
        left outer join hi.pttype p on i.pttype = p.pttype  
        left join hi.dct on (
        CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
        WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,3,2)  
        END )
        left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
        left join hi.l_council as lc on dct.providertype =lc.council
        where 
        o.vn = '${info.vn}' and ic.rcptamt != 0.00 
        group by 
        o.an,ic.income
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getClaimItem(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(``);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getAppointment(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        select
        DATE_FORMAT(a.fudate,'%Y-%m-%dT00:00:00.000Z') as fudate
        ,DATE_FORMAT(a.vstdttm,'%Y-%m-%d') as register_date
        ,o.hn
        ,concat(if(pt.pname <> '',pt.pname,f.prename),pt.fname,' ',pt.lname) as ptname
        ,(case substr(a.cln,1,1) 
        when '7' then l.labname
        when '8' then x.xryname
        when '2' then p.nameprcd
        else if(a.dscrptn = '','พบแพทย์ติดตามอาการ',a.dscrptn)
        end) as follow_detail
        ,a.fuok
        ,c.cln as clinic
        ,c.namecln as location
        ,v.advice as patient_instruction
        ,if(o.cln = '40100',concat(p_dt.prename,dentist.fname,' ',dentist.lname),concat(p_dr.prename,dct.fname,' ',dct.lname)) as participant
        ,if(o.cln = '40100',dentist.lcno,dct.lcno) as participant_reference
        ,if(o.cln = '40100',if(dentist.lcno <> '','ท.',''),if(dct.council in ('01','011'),'ว.','')) as reference_code
        ,if(o.cln = '40100',lcd.council_name,lc.council_name) as participant_type
        from 
        hi.oapp as a
        inner join hi.ovst as o on a.vn=o.vn 
        inner join hi.pt on o.hn=pt.hn 
        left join hi.l_prename as f on (CASE pt.male 
            WHEN 1 THEN 
            IF( pt.mrtlst < 6, IF( TIMESTAMPDIFF(year,pt.brthdate,NOW() ) < 15, '001', '003' ),
            IF(  TIMESTAMPDIFF(year,pt.brthdate,NOW()) < 20, '823', '831' )) 
            WHEN 2 THEN 
            IF( pt.mrtlst = 1,IF(  TIMESTAMPDIFF(year,pt.brthdate,NOW() ) < 15, '002', '004' ),
            IF ( pt.mrtlst < 6, '005', '863' )) END) = f.prename_code
        left join hi.cln as c on substr(a.cln,1,1) = substr(c.cln,1,1)
        left join hi.lab as l on  substr(a.cln,1,1) = '7' and substr(a.cln,2,3) = l.labcode 
        left join hi.xray as x on  substr(a.cln,1,1) = '8' and substr(a.cln,2,4) = x.xrycode 
        left join hi.prcd as p on  substr(a.cln,1,1) = '2' and substr(a.cln,2,4) = p.codeprcd
        left join hi.dct on (
            CASE WHEN LENGTH(a.dct) = 5 THEN dct.lcno = a.dct 
            WHEN LENGTH(a.dct) = 4 THEN dct.dct = substr(a.dct,1,2)  
            END )
        left join hi.dentist on (case length(a.dct) when 2 then a.dct = dentist.codedtt end)
        left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
        left join hi.l_prename as p_dt on dentist.pname= p_dt.prename_code
        left join hi.l_council as lc on dct.providertype =lc.council
        left join hi.l_council as lcd on dentist.providertype =lcd.council
        left join hi.visitadvice as v on o.vn=v.vn
        where o.vn='${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }

    async getImmunization(db: Knex, req: any) {
        let info:any = req.body;
        let data: any = await db.raw(`
        select 
        DATE_FORMAT(date(o.vstdttm),'%Y%m%dT%H:%i%s.000Z') as date_serv,  
        cv.vac as vaccine_code, 
        ifnull(cv.vaccine_fullname,cv.namehpt) as vaccine_name,
        e.ex_date as expire_date,
        e.lotno as lot_no,
        e.serial_no,
        cv.vaccine_route,
        (case cv.vaccine_route 
            when 'IM' then 'Injection, intramuscular'
            when 'IV' then 'Injection, intravenous'
            when 'SQ' then 'Injection, subcutaneous'
            else cv.vaccine_route
        end) as route_name,
        if(cv.vaccine_route IN ('IM','IV','Subcutaneous'),'LA','') as site_code,
        if(cv.vaccine_route IN ('IM','IV','Subcutaneous'),'Left Arm','') as site_name,
        (select hi_hsp_nm from hi.setup limit 1) as vaccine_location
        ,concat(p_dr.prename,dct.fname,' ',dct.lname) as participant
        ,dct.lcno as participant_reference
        ,if(dct.council in ('01','011'),'ว.','') as reference_code
        ,lc.council_name as participant_type
        from 
        hi.epi e 
        inner join 
        hi.ovst o on e.vn = o.vn 
        inner join hi.pt on o.hn=pt.hn 
        inner join 
        hi.hpt cv on e.vac = cv.codehpt
        LEFT OUTER JOIN 
        hi.dct on (
            CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
        left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
        left join hi.l_council as lc on dct.providertype =lc.council
        where 
        o.vn = '${info.vn}'
        UNION
        SELECT 
        date_format(o.vstdttm,'%Y%m%dT%H:%i%s.000Z') as date_serv,
        vc.stdcode as vaccine_code, 
        vc.name as vaccine_name,
        '' as expire_date,
        '' as lot_no,
        '' as serial_no,
        pd.medusage as vaccine_route,
        (case pd.medusage
            when 'IM' then 'Injection, intramuscular'
            when 'IV' then 'Injection, intravenous'
            when 'SQ' then 'Injection, subcutaneous'
            else pd.medusage
        end) as route_name,
        if(pd.medusage IN ('IM','IV','Subcutaneous'),'LA','') as site_code,
        if(pd.medusage IN ('IM','IV','Subcutaneous'),'Left Arm','') as site_name,
        (select hi_hsp_nm from hi.setup limit 1) as vaccine_location
        ,concat(p_dr.prename,dct.fname,' ',dct.lname) as participant
        ,dct.lcno as participant_reference
        ,if(dct.council in ('01','011'),'ว.','') as reference_code
        ,lc.council_name as participant_type
        from 
        hi.ovst o 
        inner join hi.pt on o.hn=pt.hn 
        inner join 
        hi.prsc pc on o.vn = pc.vn  
        inner join 
        hi.prscdt pd on pc.prscno = pd.prscno  
        inner join 
        hi.meditem m on pd.meditem = m.meditem 
        inner join 
        hi.vaccine vc on vc.meditem = m.meditem  
        LEFT OUTER JOIN 
        hi.dct on (
            CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
        left join hi.l_prename as p_dr on dct.pname= p_dr.prename_code
        left join hi.l_council as lc on dct.providertype =lc.council
        where 
        o.vn =  '${info.vn}'
        `);

        const result: any = {
            ok: true,
            text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล ',
            rows: data[0]
        }
        return result;
    }


    async list(db: Knex, req: any) {
        let info: any = req.body;
        let xlimit = 0;
        let xoffset = 0;
        let xorderby = "id";
    
        for (const property in info) { if (info[property] === "") { delete info[property]; } }
    
        if (info.limit) { xlimit = info.limit; }
        if (info.offset) { xoffset = info.offset; }
        if (info.orderby) { xorderby = info.orderby; }
    
        let criteria = " Where ";

        if (info.create_date_start == "" || info.create_date_end == "") {
            delete info.create_date_start;
            delete info.create_date_end;
          }
          if (info.create_date_start && info.create_date_end) { criteria += ` (create_date between  '${info.create_date_start}'  AND  '${info.create_date_end}') AND `; }
      
    
        if (info.id) { criteria += ` id = ${info.id} AND `; }
        if (info.hn) { criteria += ` hn = '${info.hn}' AND `; }
        if (info.vn) { criteria += ` vn = ${info.vn} AND `; }
        if (info.MessageCode) { criteria += ` MessageCode = '${info.MessageCode}' AND `; }
    
        criteria += " 1=1";
    
    
        let sql = ` select * from phr_visit_log ${criteria} order by ${xorderby} desc  limit ${xlimit} offset ${xoffset}  `;
        let datas = await db.raw(sql);
        const result: any = {
          ok: true,
          text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
          rows: datas[0]
        }
        return result;
      }

    async select_id(db: Knex, id: any) {
        let sql = `select * from phr_visit_log where id = ${id}`;
        const rs = await db.raw(sql);
        return rs;
      }

    async save (db: Knex,data:any) {
        for (const property in data) { if (data[property] === "") { delete data[property]; } }
        let saved_rs = await db('phr_visit_log')
          .insert(data);

          const datas: any = await db.raw(`select * from phr_visit_log where id = ${saved_rs}`);
    
        const result: any = {
            ok: true,
            text: "บันทึกข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล Phr Visit Log',
            rows: datas[0]
        }
        return result;
    
    }

    async update(db: Knex, id: any, data: any) {
        for (const property in data) { if (data[property] === "") { delete data[property]; } }
        
          const rows = await db('phr_visit_log')
            .where("id", id)
            .update(data);

          const datas = await db.raw(`select * from phr_visit_log where id = ${id}`);

          const result: any = {
            ok: true,
            text: "แก้ไขข้อมูลเสร็จเรียบร้อยแล้ว",
            title: 'ข้อมูล Phr Visit Log',
            rows: datas[0]
          };
          return result;
      }
}